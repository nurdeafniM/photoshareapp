const mongoose = require('mongoose')
let Schema = mongoose.Schema
let userSchema= new Schema({
    userId:Number,
    email:String ,
    password: String,
    name:String,
    createAt:Date,
    updateAt:Date,
})
module.exports={userSchema}