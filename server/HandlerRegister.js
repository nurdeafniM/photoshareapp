const {userSchema}= require('./SchemaRegister')
const mongoose= require('mongoose')
mongoose.connect('mongodb://localhost/photoShareApp',{
    useNewUrlParser: true,
    useUnifiedTopology: true
});
const user= mongoose.model('users',userSchema)

const getAllUser= async function(request,h){
    return user.find()
}
const postUser= async function(request, h){
    const idUser= await user.aggregate([ 
        {
            $group:{
                userId:'max',
                maxId:{
                    $max:'$userId',
                }, 
                email:{
                    $push:'$email'
                }
            }
        }
    ])
    let nextId=idUser[0].maxId+1
    Object.assign(request.payload,{userId:nextId})
    await user.insertMany([request.payload])
    return h.response(request.payload).code(201)
}
module.exports={getAllUser,postUser}