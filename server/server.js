const hapi = require ('@hapi/hapi')
const Joi = require('@hapi/joi');
const Inert = require('@hapi/inert');
const Bcrypt = require('bcrypt');
const Qs = require('qs')
const CatboxRedis = require('@hapi/catbox-redis');
const mongoose = require('mongoose');
const {getAllUser,getUserById,postUser}= require('./HandlerRegister')
mongoose.set('debug',true)


const users = {
    nurDe: {
        username: 'nurDe',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret' 
        name: 'Nur De',
        id: '2133d32a'
    }
};
const validate = async (request, username, password) => {
    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }
    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };
    return { isValid, credentials };
};

const init = async()=>{
    const server= hapi.server({
        port: 9099,
        host: '0.0.0.0', //20.4.12.217
        query:{
            parser:(query) => Qs.parse(query)
        },
        cache: [
            {
                name: 'my_cache',
                provider: {
                    constructor: CatboxRedis,
                    options: {
                        partition : 'my_cached_data',
                        host: '127.0.0.1',
                        port: 6379,
                        database: 0
                    }
                }
            }
        ]

    })
    await server.register(require('@hapi/basic'));
    await server.register({
        plugin: require('hapi-pino'),
        options: { 
          prettyPrint: process.env.NODE_ENV !== 'production',
           stream:'./server.log',
           redact: ['req.headers.authorization']
        }
      })
    server.auth.strategy('simple', 'basic', { validate });
    await server.register(Inert)
    //get one id in file pets.json
    
    server.route({
        method: 'GET',
        path: '/user',
        handler: getAllUser,
        options: {
            auth: 'simple'
        },
    })
    server.route({
        method: 'POST',
        path: '/register',
        handler: postUser
    })
    process.on('unhandledRejection', (err) => {
        console.log(err)
        process.exit(1)
    })
     await server.start()
    console.log('Server running on %s', server.info.uri)
    return server
}
init();