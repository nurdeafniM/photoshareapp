import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    containerApp : {
        flex: 1,
    },
    containerLogo : {
        flexGrow: 1,
        justifyContent:'flex-end',
        alignItems: 'center'
        },
    logoText : {

        marginVertical: 15,
        fontSize:18,
        color:'rgba(255, 255, 255, 0.7)'

        },

})

module.exports={styles}