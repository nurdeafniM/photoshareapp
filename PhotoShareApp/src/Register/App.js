import React, { Component } from 'react';
import {
StyleSheet,
View,
StatusBar
} from 'react-native';
import Routes from '../Register/Routes';
import {styles} from '../Register/Styles/Styles'

export default class App extends Component {
    render() {
        return (
        <View style={styles.containerApp}>
            <StatusBar
                backgroundColor="#1c313a"
                barStyle="light-content"
                />
                <Routes/>
                </View>
                );
            }
        }
