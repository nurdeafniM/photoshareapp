import React, { Component } from 'react';
import {
StyleSheet,
Text,
View,
Image
} from 'react-native';
import {styles} from '../Styles/Styles'

export default class Logo extends Component{
    render(){
        return(
        <View style={styles.containerLogo}>
            <Image 
                style={{width:40, height: 70}}
                source={require('../Image/twitter.png')}/>
                <Text style={styles.logoText}>Welcome to Photo Share App.</Text>
                </View>
                )
            }
        }